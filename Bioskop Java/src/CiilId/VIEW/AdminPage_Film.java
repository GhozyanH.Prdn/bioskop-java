/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CiilId.VIEW;

import CiilId.CRUD.CRUD_Film;
import CiilId.CRUD.CRUD_Pesan;
import java.awt.HeadlessException;
import java.awt.Image;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 *
 */
public class AdminPage_Film extends javax.swing.JFrame {

    private String id, tanggal, judul, genre, sutradara, durasi, harga;
    private Connection con;
    private Statement stat;
    private ResultSet res;
    private PreparedStatement pst;
    private ResultSet rs;

    /**
     * Creates new form StaffPage_Film
     */
    public AdminPage_Film() {
        initComponents();
        dataTable();
        ShowComboDB();
    }

    private void koneksi() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/dbcillid", "root", "");
            stat = con.createStatement();
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    private void dataTable() {
        DefaultTableModel tabel = new DefaultTableModel();
        tabel.addColumn("id");
        tabel.addColumn("judul"); 
        tabel.addColumn("harga");
        tabel.addColumn("genre");
        tabel.addColumn("sutradara");
        tabel.addColumn("tanggal");
        tabel.addColumn("durasi");
        

        tabelFilm.setModel(tabel);

        try {
            koneksi();
            String sql = "SELECT * FROM film";
            res = stat.executeQuery(sql);

            while (res.next()) {
                tabel.addRow(new Object[]{
                    res.getString(1),
                    res.getString(3),
                    res.getString(2),
                    res.getString(4),
                    res.getString(5),
                    res.getString(6),
                    res.getString(7),
                });
            }
        } catch (SQLException e) {
        }
    }

    private void ShowComboDB() {

        DefaultComboBoxModel data2 = new DefaultComboBoxModel();

        try {
            CRUD_Pesan CP = new CRUD_Pesan();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton4 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        inputjudul = new javax.swing.JTextField();
        inputtanggal = new com.toedter.calendar.JDateChooser();
        inputgenre = new javax.swing.JTextField();
        input_sutradara = new javax.swing.JTextField();
        id_film_label = new javax.swing.JTextField();
        inputdurasi = new javax.swing.JTextField();
        inputharga = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelFilm = new javax.swing.JTable();
        jLabel12 = new javax.swing.JLabel();

        jButton4.setText("Kembali");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        inputjudul.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inputjudulActionPerformed(evt);
            }
        });
        jPanel1.add(inputjudul, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 80, 145, -1));
        inputjudul.getAccessibleContext().setAccessibleName("");
        inputjudul.getAccessibleContext().setAccessibleParent(this);

        inputtanggal.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                inputtanggalPropertyChange(evt);
            }
        });
        jPanel1.add(inputtanggal, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, 140, -1));

        nama_admin.setEditable(false);
        nama_admin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nama_adminActionPerformed(evt);
            }
        });
        jPanel1.add(nama_admin, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 90, -1));
        jPanel1.add(inputgenre, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 110, 145, -1));
        jPanel1.add(input_sutradara, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 50, 110, -1));

        id_film_label.setText("0");
        jPanel1.add(id_film_label, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 140, 20, -1));
        jPanel1.add(inputdurasi, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 80, 110, -1));
        jPanel1.add(inputharga, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 110, 110, 30));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userselect/image/simpan 2.jpg"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 190, 60, 30));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userselect/image/hapus.jpg"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 190, 70, 30));

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userselect/image/edit4.jpg"))); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 190, 70, 30));

        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-100, 200, 30, 20));

        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(21, 10, 30, 20));

        tabelFilm.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Tanggal", "Judul", "Genre", "Sutradara", "Durasi", "Harga"
            }
        ));
        tabelFilm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelFilmMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelFilm);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 560, 110));

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/userselect/image/edit film.jpg"))); // NOI18N
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 360));

        jPanel3.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 13, 579, 360));

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -10, 580, 370));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tabelFilmMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelFilmMouseClicked
        DefaultTableModel tableFilm = (DefaultTableModel) tabelFilm.getModel();
        int seectedrow = tabelFilm.getSelectedRow();
        
        id_film_label.setText((String) tableFilm.getValueAt(tabelFilm.getSelectedRow(), 0));
        inputharga.setText((String) tableFilm.getValueAt(tabelFilm.getSelectedRow(), 2));
        inputjudul.setText((String) tableFilm.getValueAt(tabelFilm.getSelectedRow(), 1));
        inputgenre.setText((String) tableFilm.getValueAt(tabelFilm.getSelectedRow(), 3));
        input_sutradara.setText((String) tableFilm.getValueAt(tabelFilm.getSelectedRow(), 4));
        inputtanggal.setDateFormatString((String) tableFilm.getValueAt(tabelFilm.getSelectedRow(), 5));
//        try {
//            Date date = new SimpleDateFormat("yyyy-MM-dd").parse((String)tableFilm.getValueAt(seectedrow, 4).toString());
//        } catch (ParseException ex) {
//            Logger.getLogger(AdminPage_Film.class.getName()).log(Level.SEVERE, null, ex);
//        }
        inputdurasi.setText((String) tableFilm.getValueAt(tabelFilm.getSelectedRow(), 6));
        
        
    }//GEN-LAST:event_tabelFilmMouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        new PageAdmin().show();
        this.dispose();
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        // TODO add your handling code here:
        new PageAdmin().show();
        this.dispose();
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        harga = inputharga.getText();
        judul = inputjudul.getText();
        genre = inputgenre.getText();
        sutradara = input_sutradara.getText();
        tanggal = ((JTextField)inputtanggal.getDateEditor().getUiComponent()).getText();
        durasi = inputdurasi.getText();
        

        try{
            CRUD_Film CF = new CRUD_Film();
            if(CF.ubahData(id_film_label.getText(),harga,judul, genre, sutradara, tanggal, durasi)){
                JOptionPane.showMessageDialog(null, "Berhasil Disimpan");
                dataTable();
            }else{
                JOptionPane.showMessageDialog(null, "Terjadi Kesalahan");
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try{
            CRUD_Film CF = new CRUD_Film();
            if(CF.hapusData(id_film_label.getText())){
                JOptionPane.showMessageDialog(null, "Berhasil Dihapus");
                dataTable();
            }else{
                JOptionPane.showMessageDialog(null, "Terjadi Kesalahan");
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        harga = inputharga.getText();
        judul = inputjudul.getText();
        genre= inputgenre.getText();
        sutradara = input_sutradara.getText();
        tanggal = ((JTextField)inputtanggal.getDateEditor().getUiComponent()).getText();
        durasi = inputdurasi.getText();
        
        

        try {
            CRUD_Film CF  = new CRUD_Film(harga,judul,genre,sutradara,tanggal,durasi);
            if (CF.masukkanData()) {
                JOptionPane.showMessageDialog(null, "Berhasil", "Status", JOptionPane.INFORMATION_MESSAGE, null);
                System.out.println("Berhasil");
                dataTable();
            } else {
                JOptionPane.showMessageDialog(null, "Gagal", "Status", JOptionPane.ERROR_MESSAGE, null);
            }
        } catch (SQLException ex) {
            //Logger.getLogger(AksesDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void inputjudulActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inputjudulActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_inputjudulActionPerformed

    private void inputtanggalPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_inputtanggalPropertyChange
        if(inputtanggal.getDate()!=null){
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
            tanggal=format.format(inputtanggal.getDate());
        }
    }//GEN-LAST:event_inputtanggalPropertyChange

    private void nama_adminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nama_adminActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nama_adminActionPerformed
public void showTableData(){
    try{
        con = DriverManager.getConnection("jdbc:mysql://localhost/dbcillid","root","");
        String sql = " SELECT FROM film";
        pst = con.prepareStatement(sql);
         rs=pst.executeQuery();
        
    }   catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
       Logger.getLogger(AdminPage_Film.class.getName()).log(Level.SEVERE, null, ex);
        
    }

}
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AdminPage_Film.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AdminPage_Film.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AdminPage_Film.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AdminPage_Film.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new AdminPage_Film().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField id_film_label;
    private javax.swing.JTextField input_sutradara;
    private javax.swing.JTextField inputdurasi;
    private javax.swing.JTextField inputgenre;
    private javax.swing.JTextField inputharga;
    private javax.swing.JTextField inputjudul;
    private com.toedter.calendar.JDateChooser inputtanggal;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    public static final javax.swing.JTextField nama_admin = new javax.swing.JTextField();
    private javax.swing.JTable tabelFilm;
    // End of variables declaration//GEN-END:variables
// Resize Image Function
}

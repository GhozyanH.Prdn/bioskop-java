    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CiilId.CRUD;

import Cill_id.DBConection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Ghozyan Hilman Pradana <ozy-dev.web.id>
 */
public class CRUD_Pelanggan {
    private Connection koneksi;
    private PreparedStatement ps;
    private DBConection kdb = new DBConection();
    private String id_pelanggan, kasir, nomor_studio, judul, waktu_pemesanan,jam_tayang, baris, nomor_bangku, harga;


    public CRUD_Pelanggan(String kasir, String nomor_studio, String judul, String waktu_pemesanan, String jam_tayang, String baris, String nomor_bangku, String harga) {
        this.kasir = kasir;
        this.nomor_studio = nomor_studio;
        this.judul = judul;
        this.waktu_pemesanan = waktu_pemesanan;
        this.jam_tayang = jam_tayang;
        this.baris = baris;
        this.nomor_bangku = nomor_bangku;
        this.harga = harga;
    }

    
    public void lihatNData() throws SQLException {
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            String kueri = "SELECT * FROM pelanggan";
            ps = koneksi.prepareStatement(kueri);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("pelanggan");
                System.out.println("id: " + rs.getString(1));
                System.out.println("nama: " + rs.getString(2));
                System.out.println("nomor_studio: " + rs.getString(3));
                System.out.println("judul: " + rs.getString(4));
                System.out.println("waktu_pemesanan: " + rs.getString(5));
                System.out.println("jam_tayang: " + rs.getString(6));
                System.out.println("baris: " + rs.getString(7));
                System.out.println("nomor_bangku: " + rs.getString(8));
                System.out.println("harga: " + rs.getString(9));
                System.out.println("");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            ps.close();
        }
    }
    
    public boolean masukkanData() throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        String insertTableSQL = "INSERT INTO pelanggan"
                + "(kasir, nomor_studio, judul, waktu_pemesanan, jam_tayang,  baris, nomor_bangku, harga) VALUES"
                + "(?,?,?,?,?,?,?,?)";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.kasir);
            ps.setString(2, this.nomor_studio);
            ps.setString(3, this.judul);
            ps.setString(4, this.waktu_pemesanan);
            ps.setString(5, this.jam_tayang);
            ps.setString(6, this.baris);
            ps.setString(7, this.nomor_bangku);
            ps.setString(8, this.harga);
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke-5: menterjemahkan hasil yang dikembalikan
    //dari bentuk integer ke dalam bentuk boolean sebagai representasi keberhasilan eksekusi
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean hapusData(String id_pelanggan) throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        this.id_pelanggan = id_pelanggan;
        String deleteTableSQL = "DELETE from pelanggan WHERE id = ? ";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(deleteTableSQL);
            ps.setString(1, this.id_pelanggan);
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke 5
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean ubahData(String id_pelanggan, String kasir_baru, String nomor_studio_baru, String judul_baru, String waktu_pemesanan_baru, String jam_tayang_baru, String baris_baru, String nomor_bangku_baru, String harga_baru)
            throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        String updateTableSQL = "UPDATE pelanggan SET kasir = ?,  nomor_studio = ?,"
                + " judul = ?, waktu_pemesanan = ?, jam_tayang =?,"
                + " baris = ?, nomor_bangku = ?, harga = ? WHERE id = ?";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(updateTableSQL);
            ps.setString(9, id_pelanggan);
            ps.setString(1, kasir_baru);
            ps.setString(2, nomor_studio_baru);
            ps.setString(3, judul_baru);
            ps.setString(4, waktu_pemesanan_baru);
            ps.setString(5, jam_tayang_baru);
            ps.setString(6, baris_baru);
            ps.setString(7, nomor_bangku_baru);
            ps.setString(8, harga_baru);
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke 5
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }
}

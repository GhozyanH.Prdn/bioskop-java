/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CiilId.CRUD;

import Cill_id.DBConection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Ghozyan Hilman Pradana <ozy-dev.web.id>
 */
public class CRUD_Studio {
    private Connection koneksi;
    private PreparedStatement ps;
    private DBConection kdb = new DBConection();
    private String id_studio, nomor_studio, judul_film, jam_tayang;
    
    public CRUD_Studio(){
    
    }

    public CRUD_Studio(String nomor_studio, String judul_film, String jam_tayang) {
        this.nomor_studio = nomor_studio;
        this.judul_film = judul_film;
        this.jam_tayang = jam_tayang;
    }
    
    public void lihatNData() throws SQLException {
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            String kueri = "SELECT * FROM studio";
            ps = koneksi.prepareStatement(kueri);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("studio");
                System.out.println("id: " + rs.getString(1));
                System.out.println("nomor studio: " + rs.getString(2));
                System.out.println("Judul Film: " + rs.getString(3));
                System.out.println("Jam Tayng: " + rs.getString(4));
                System.out.println("");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            ps.close();
        }
    }
    
    public boolean masukkanData() throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        String insertTableSQL = "INSERT INTO studio"
                + "(nomor_studio, judul, jam_tayang) VALUES"
                + "(?,?,?)";
        try {
        //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
        //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
        //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.nomor_studio);
            ps.setString(2, this.judul_film);
             ps.setString(3, this.jam_tayang);
        //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
        //langkah ke 6
            ps.close();
        }
        //langkah ke-5: menterjemahkan hasil yang dikembalikan
        //dari bentuk integer ke dalam bentuk boolean sebagai representasi keberhasilan eksekusi
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean hapusData(String id_studio) throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        this.id_studio = id_studio;
        String deleteTableSQL = "DELETE from studio WHERE id = ? ";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(deleteTableSQL);
            ps.setString(1, this.id_studio);
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke 5
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean ubahData(String idStudio, String nomor_studio_baru, String judul_film_baru, String jam_tayang_baru)
            throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        String updateTableSQL = "UPDATE studio SET nomor_studio = ?, judul = ?, jam_tayang = ?"
                + " WHERE id = ?";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(updateTableSQL);
            ps.setString(4, idStudio);
            ps.setString(1, nomor_studio_baru);
            ps.setString(2, judul_film_baru);
            ps.setString(3, jam_tayang_baru);
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke 5
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public void comboData(DefaultComboBoxModel model){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM film";
       
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                model.addElement(rs.getString(3));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public boolean resetKursi(String nomor_studio, String judul, String jam_tayang)
            throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        String updateTableSQL = "UPDATE studio SET a1 = 0, a2 = 0, a3 = 0, a4 = 0,"
                + " b1 = 0, b2 = 0, b3 = 0, b4 = 0,"
                + " c1 = 0, c2 = 0, c3 = 0, c4 = 0"
                + " WHERE nomor_studio = ? AND judul = ? AND jam_tayang = ?";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(updateTableSQL);
            ps.setString(1, nomor_studio);
            ps.setString(2, judul);
            ps.setString(3, jam_tayang);
            
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke 5
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }
}

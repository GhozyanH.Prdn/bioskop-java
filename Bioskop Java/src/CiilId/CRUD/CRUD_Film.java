/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CiilId.CRUD;

import Cill_id.DBConection;
import com.toedter.calendar.JDateChooser;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 *
 */
public class CRUD_Film {
    private Connection koneksi;
    private PreparedStatement ps;
    private DBConection kdb = new DBConection();
    private String id_film, judul,genre, sutradara, kategori, durasi,tanggal,harga;

    public CRUD_Film() {
    }

    public CRUD_Film(String harga, String judul,String genre, String sutradara,String tanggal, String durasi ) {
        this.harga = harga;
        this.judul = judul;
        this.genre = genre;
        this.sutradara = sutradara;
        this.tanggal = tanggal;
        this.durasi = durasi;

        
    }

    
    
    public void lihatNData() throws SQLException {
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            String kueri = "SELECT * FROM film";
            ps = koneksi.prepareStatement(kueri);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                System.out.println("bioskop");
                System.out.println("id: " + rs.getString(1));
                System.out.println("judul: " + rs.getString(3));
                System.out.println("genre: " + rs.getString(4));
                System.out.println("sutradara: " + rs.getString(5));
                System.out.println("tanggal: " + rs.getString(6));
                System.out.println("durasi: " + rs.getString(7));
                System.out.println("harga: " + rs.getString(2));
                System.out.println("");
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            ps.close();
        }
    }
    
    public boolean masukkanData() throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        String insertTableSQL = "INSERT INTO film"
                + "(harga, judul, genre, sutradara, tanggal, durasi) VALUES"
                + "(?,?,?,?,?,?)";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setString(1, this.harga);
            ps.setString(2, this.judul);
            ps.setString(3, this.genre);
            ps.setString(4, this.sutradara);
            ps.setString(5, this.tanggal);
            ps.setString(6, this.durasi);
            
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke-5: menterjemahkan hasil yang dikembalikan
    //dari bentuk integer ke dalam bentuk boolean sebagai representasi keberhasilan eksekusi
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean hapusData(String id_film) throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        this.id_film = id_film;
        String deleteTableSQL = "DELETE from film WHERE id = ? ";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(deleteTableSQL);
            ps.setString(1, this.id_film);
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke 5
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean ubahData(String id_film, String harga_baru, String judul_baru, String genre_baru, String sutradara_baru, String tanggal_baru, String durasi_baru)
            throws SQLException {
    //deklarasi connection dan preparedStatement
        Connection dbConnection = null;
        PreparedStatement ps = null;
        int rowAffect = 0;
        String updateTableSQL = "UPDATE film SET harga = ?, judul = ?, genre = ?, sutradara = ?,"
                + " tanggal = ?, durasi = ?"
                + " WHERE id = ?";
        try {
    //buka koneksi saat objek dari desa ninja dibentuk
            kdb.bukaKoneksi();
    //inisialisasi dbConnection dari objek Connection
            dbConnection = kdb.getConn();
    //Langkah ke 4 bagian 1
            ps = dbConnection.prepareStatement(updateTableSQL);
            ps.setString(7, id_film);
            ps.setString(1, harga_baru);
            ps.setString(2, judul_baru);
            ps.setString(3, genre_baru);
            ps.setString(4, sutradara_baru);
            ps.setString(5, tanggal_baru );
            ps.setString(6, durasi_baru);
            

            
    //langkah 4: eksekusi query
            rowAffect = ps.executeUpdate();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
    //langkah ke 6
            ps.close();
        }
    //langkah ke 5
        if (rowAffect > 0) {
            return true;
        } else {
            return false;
        }
    }


//    public boolean masukkanData(String tanggal, String judul, String genre, String sutradara, String durasi, String harga) {
//        try{
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            String  sql = "insert into film values("+0+",'"+judul+"','"+genre+"','"+sutradara+"','"+tanggal+"','"+durasi+"','"+harga+"','A1')";
//            Connection con = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/dbcillid","root","");
//            Statement stat = (Statement) con.createStatement();
//            
//            stat.executeUpdate(sql);
//            stat.close();
//            JOptionPane.showMessageDialog(null, "Update Berhasil Dilakukan");
//            con.close();
//        }catch(ClassNotFoundException | InstantiationException | IllegalAccessException| SQLException e){
//            System.out.println(e);
//        }
//        return true;
//    }

}
